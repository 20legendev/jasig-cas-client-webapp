package vn.tcbs.sso.client.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.cas.authentication.CasAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

	@RequestMapping("/")
	public ModelAndView hello(HttpServletRequest req) {
		String user = req.getRemoteUser();
		if (req.getUserPrincipal() != null) {
			CasAuthenticationToken token = (CasAuthenticationToken) req.getUserPrincipal();
			System.out.println("KIEN " + token);
			// AttributePrincipal principal = (AttributePrincipal)
			// req.getUserPrincipal();

			// Map attributes = principal.getAttributes();
			// user += attributes.size();
		}
		return new ModelAndView("hello", "message", "Hello " + user);
	}
}